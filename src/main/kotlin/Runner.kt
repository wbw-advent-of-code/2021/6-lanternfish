const val input = "src/main/resources/input.txt"

fun lanternfishPart1() {
    println(LanternfishSim(input).solvePart1())
}

fun lanternfishPart2() {
    println(LanternfishSim(input).solvePart2())

}

fun main() {
    lanternfishPart2()
}