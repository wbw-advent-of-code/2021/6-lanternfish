import java.io.File

class LanternfishSim(input: String) {

    private val fishiesPerDay: LongArray = parseInput(File(input).readText())

    fun solvePart1(): Long =
        simulateDays(80)

    fun solvePart2(): Long =
        simulateDays(256)

    private fun simulateDays(days: Int): Long {
        repeat(days) {
            fishiesPerDay.rotateLeftInPlace()
            fishiesPerDay[6] += fishiesPerDay[8]
        }
        return fishiesPerDay.sum()
    }

    private fun LongArray.rotateLeftInPlace() {
        val newSpawnees = first()
        // Shift all elements 1 space to the left
        this.copyInto(this, startIndex = 1)
        // Add all new fish at the end
        this[this.lastIndex] = newSpawnees
    }

    // Parse input in array grouping fish by the number of days
    // E.g.: 3,4,3,1,2
    // [ 0, 1, 1, 2, 1, 0, 0, 0, 0]
    // => 1 fish with 1 day, 1 with 2 days, ....
    private fun parseInput(input: String): LongArray =
        LongArray(9).apply {
            input.split(",").map { it.toInt() }.forEach { this[it] += 1L }
        }
}