import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class TestInputRunner {
    val input = "src/test/resources/input.txt"

    @Test
    internal fun `test 80 days`() {
        assertThat(LanternfishSim(input).solvePart1())
            .isEqualTo(5934)
    }
    @Test
    internal fun `test 256 days`() {
        assertThat(LanternfishSim(input).solvePart2())
            .isEqualTo(26984457539)
    }
}